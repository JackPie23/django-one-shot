from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo": todo
    }
    return render(request, "todo/list.html", context)


def list_detail(request, id):
    list_details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": list_details,
    }
    return render(request, "todo/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todo/create.html", context)


def todo_list_update(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoForm(instance=todo_instance)
    context = {
        "edit_form": form,
        "todo_object": todo_instance
    }

    return render(request, "todo/edit.html", context)


def todo_list_delete(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todo/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            items = form.save()
            return redirect("todo_list_detail", id=items.list.id)
    else:
        form = TodoItemForm()
    context = {
        "item_form": form
    }
    return render(request, "todo/createitem.html", context)


def todo_item_update(request, id):
    todo_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_instance.list.id)
    else:
        form = TodoItemForm(instance=todo_instance)
    context = {
        "edit_item_form": form,
        "item_object": todo_instance
    }
    return render(request, "todo/edititem.html", context)
